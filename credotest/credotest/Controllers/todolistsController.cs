﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using credotest.DataAccess;
using credotest.Models;

namespace credotest.Controllers
{
	public class todolistsController : ApiController
	{
		private readonly IToDoListItemRepository todoRepo;

		public todolistsController(IToDoListItemRepository repo)
		{
			todoRepo = repo;
		}

		// GET: api/todolists
		[ResponseType(typeof(IList<todolist>))]
		public IHttpActionResult Gettodolists()
		{
			return Ok(todoRepo.GetAll());
		}

		// GET: api/todolists/5
		[ResponseType(typeof(todolist))]
		public IHttpActionResult Gettodolist(int id)
		{
			var todolist = todoRepo.Get(id);
			if (todolist == null)
			{
				return NotFound();
			}

			return Ok(todolist);
		}

		// PUT: api/todolists/5
		[ResponseType(typeof(void))]
		public IHttpActionResult Puttodolist(int id, todolist todolist)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (id != todolist.Id)
			{
				return BadRequest();
			}

			try
			{
				todolist = todoRepo.Save(todolist);
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!todolistExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return StatusCode(HttpStatusCode.NoContent);
		}

		// POST: api/todolists
		[ResponseType(typeof(todolist))]
		public IHttpActionResult Posttodolist(todolist todolist)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			todoRepo.Save(todolist);

			return CreatedAtRoute("DefaultApi", new
			{
				id = todolist.Id
			}, todolist);
		}

		// DELETE: api/todolists/5
		[ResponseType(typeof(int))]
		public IHttpActionResult Deletetodolist(int id)
		{
			todoRepo.Delete(id);

			return Ok(id);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				//dispose of any resources
			}
			base.Dispose(disposing);
		}

		private bool todolistExists(int id)
		{
			return todoRepo.Get(id) != null;
		}
	}
}