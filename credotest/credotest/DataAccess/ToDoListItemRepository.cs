﻿using System.Collections.Generic;
using System.Linq;
using credotest.Models;

namespace credotest.DataAccess
{
	public interface IToDoListItemRepository
	{
		IList<todolist> GetAll();
		todolist Get(int id);
		todolist Save(todolist todoItem);
		todolist ChangeStatus(int id);
		todolist Delete(int id);
	}

	public class ToDoListItemRepository : IToDoListItemRepository
	{
		public IList<todolist> GetAll()
		{
			using (var ctx = new todolistEntities())
			{
				return ctx.todolists.Where(td => td.Id != 0).ToList();
			}
		}

		public todolist Get(int id)
		{
			using (var ctx = new todolistEntities())
			{
				return ctx.todolists.SingleOrDefault(td => td.Id == id);
			}
		}

		public todolist Save(todolist todoItem)
		{
			using (var ctx = new todolistEntities())
			{
				if (todoItem.Id <= 0)
				{
					ctx.todolists.Add(todoItem);
				}
				else
				{
					var existingItem = Get(todoItem.Id);
					existingItem.Description = todoItem.Description;
					existingItem.IsComplete = todoItem.IsComplete;
					ctx.Entry(existingItem).State = System.Data.Entity.EntityState.Modified;
				}
				ctx.SaveChanges();
				return todoItem;
			}
		}

		public todolist ChangeStatus(int id)
		{
			using (var ctx = new todolistEntities())
			{
				var todoItem = ctx.todolists.SingleOrDefault(td => td.Id == id);
				if (todoItem != null)
				{
					todoItem.IsComplete = !todoItem.IsComplete;
					ctx.SaveChanges();
				}
				return todoItem;
			}
		}

		public todolist Delete(int id)
		{
			using (var ctx = new todolistEntities())
			{
				var todoItem = ctx.todolists.SingleOrDefault(td => td.Id == id);
				if (todoItem != null)
				{
					ctx.todolists.Remove(todoItem);
					ctx.SaveChanges();
				}
				return todoItem;
			}
		}

		public void ResetData()
		{
			using (var ctx = new todolistEntities())
			{
				var todos = ctx.todolists.Where(td => td.Id != 0).ToList();
				foreach (var todoItem in todos)
				{
					ctx.todolists.Remove(todoItem);
				}

				var newItem1 = new todolist
				{
					Description = "Test 1",
					IsComplete = false
				};

				var newItem2 = new todolist
				{
					Description = "Test 2",
					IsComplete = true
				};

				ctx.todolists.AddRange(new todolist[] { newItem1, newItem2 });

				ctx.SaveChanges();

				//TODO: unfortunately, this does not reset the IDs, so tests will fail that rely on IDs
			}
		}
	}
}