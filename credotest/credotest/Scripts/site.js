﻿function getAll() {
    var result;
    $.ajax({
        url: '/api/todolists',
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (data) {
            result = data;
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
    return result;
}

function getOne(id) {
    var result;
    $.ajax({
        url: '/api/todolists/' + id,
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (data) {
            result = data;
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
    return result;
}

function updateOne(id) {
    var todoItem = {
        Id: id,
        Description: $("#txtDescription_" + id).val(),
        IsComplete: $("#chkIsComplete_" + id + ":checked").length > 0 ? true : false
    };

    var result;
    $.ajax({
        url: '/api/todolists/' + id,
        type: 'PUT',
        data: JSON.stringify(todoItem),
        contentType: "application/json;charset=utf-8",
        dataType: 'json',
        async: false,
        success: function (data) {
            result = data;
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
    return result;
}

function saveOne() {
    var todoItem = {
        Description: $("#txtDescription").val(),
        IsComplete: $("#chkIsComplete:checked").length > 0 ? true : false
    };

    var result;
    $.ajax({
        url: '/api/todolists',
        type: 'POST',
        data: JSON.stringify(todoItem),
        contentType: "application/json;charset=utf-8",
        dataType: 'json',
        async: false,
        success: function (data) {
            result = data;
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
    return result;
}

function deleteOne(id) {
    var result;
    $.ajax({
        url: '/api/todolists/' + id,
        type: 'DELETE',
        data: { id: id },
        contentType: "application/json;charset=utf-8",
        dataType: 'json',
        async: false,
        success: function (data) {
            result = data;
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
    return result;
}
