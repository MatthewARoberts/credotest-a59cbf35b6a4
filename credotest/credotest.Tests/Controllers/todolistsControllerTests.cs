﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Results;
using credotest.Controllers;
using credotest.DataAccess;
using credotest.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

//TODO
/*
 * All tests call ResetData first which deletes all data and adds two well-known records back.
 * Unfortunately, the Web API only exposes IDs at the moment, so these tests would fail
 * even if an MDF file were not used as the backstore. But, adding new Get methods to accept a string
 * rather than ID would be quite simple.
 */

namespace credotest.Tests.Controllers
{
	[TestClass]
	public class todolistsControllerTest
	{
		private void ResetData()
		{
			var repo = new ToDoListItemRepository();
			repo.ResetData();
		}

		[TestMethod]
		public void GetAll()
		{
			//var repo = new Mock<IToDoListItemRepository>();
			var repo = new ToDoListItemRepository();

			var controller = new todolistsController(repo);

			var result = controller.Gettodolists();

			Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<IList<todolist>>));
		}

		[TestMethod]
		public void GetExisting()
		{
			ResetData();

			//var repo = new Mock<IToDoListItemRepository>();
			var repo = new ToDoListItemRepository();

			var controller = new todolistsController(repo);

			var result = controller.Gettodolist(1);

			Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<IList<todolist>>));
		}

		[TestMethod]
		public void GetNonExisting()
		{
			ResetData();

			//var repo = new Mock<IToDoListItemRepository>();
			var repo = new ToDoListItemRepository();

			var controller = new todolistsController(repo);

			var result = controller.Gettodolist(3);

			Assert.IsInstanceOfType(result, typeof(NotFoundResult));
		}

		[TestMethod]
		public void SaveNew()
		{
			ResetData();

			//var repo = new Mock<IToDoListItemRepository>();
			var repo = new ToDoListItemRepository();

			var controller = new todolistsController(repo);

			var todoItem = new todolist
			{
				Description = "TestTestTest",
				IsComplete = false
			};

			var result = controller.Posttodolist(todoItem);

			Assert.IsInstanceOfType(result, typeof(CreatedAtRouteNegotiatedContentResult<todolist>));

			var allItems = repo.GetAll();
			Assert.IsTrue(allItems.Any(td => td.Description == "TestTestTest"));
		}

		[TestMethod]
		public void SaveNewInvalid()
		{
			ResetData();

			//var repo = new Mock<IToDoListItemRepository>();
			var repo = new ToDoListItemRepository();

			var controller = new todolistsController(repo);

			var todoItem = new todolist
			{
				Description = null,
				IsComplete = false
			};

			var result = controller.Posttodolist(todoItem);

			Assert.IsInstanceOfType(result, typeof(InvalidModelStateResult));

			var allItems = repo.GetAll();
			Assert.IsTrue(allItems.Count == 2);
		}

		[TestMethod]
		public void UpdateExisting()
		{
			ResetData();

			//var repo = new Mock<IToDoListItemRepository>();
			var repo = new ToDoListItemRepository();

			var existingItem = repo.Get(1);
			existingItem.Description += " - Updated";

			var controller = new todolistsController(repo);

			var result = controller.Puttodolist(1, existingItem);

			Assert.IsInstanceOfType(result, typeof(StatusCodeResult));

			var updatedItem = repo.Get(1);
			Assert.IsTrue(updatedItem.Description == "Test 1 - Updated");
		}


		public void UpdateExistingInvalid()
		{
			ResetData();

			//var repo = new Mock<IToDoListItemRepository>();
			var repo = new ToDoListItemRepository();

			var existingItem = repo.Get(1);
			existingItem.Description = null;

			var controller = new todolistsController(repo);

			var result = controller.Puttodolist(1, existingItem);

			Assert.IsInstanceOfType(result, typeof(InvalidModelStateResult));

			var updatedItem = repo.Get(1);
			Assert.IsTrue(updatedItem.Description == "Test 1");
		}

		public void UpdateExistingIdMismatch()
		{
			ResetData();

			//var repo = new Mock<IToDoListItemRepository>();
			var repo = new ToDoListItemRepository();

			var existingItem = repo.Get(1);
			existingItem.Description += " - Updated";

			var controller = new todolistsController(repo);

			var result = controller.Puttodolist(2, existingItem);

			Assert.IsInstanceOfType(result, typeof(BadRequestResult));

			var updatedItem = repo.Get(1);
			Assert.IsTrue(updatedItem.Description == "Test 1");
		}

		[TestMethod]
		public void DeleteExisting()
		{
			ResetData();

			//var repo = new Mock<IToDoListItemRepository>();
			var repo = new ToDoListItemRepository();

			var controller = new todolistsController(repo);

			var result = controller.Deletetodolist(1);

			Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<int>));

			var deletedItem = repo.Get(1);
			Assert.IsNull(deletedItem);
		}
	}
}
