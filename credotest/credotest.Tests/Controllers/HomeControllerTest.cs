﻿using System.Web.Mvc;
using credotest.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace credotest.Tests.Controllers
{
	[TestClass]
	public class HomeControllerTest
	{
		[TestMethod]
		public void Index()
		{
			var controller = new HomeController();

			var result = controller.Index() as ViewResult;

			Assert.IsNotNull(result);
		}
	}
}
